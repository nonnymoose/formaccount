# formaccount
Google Forms account picker

Allows you to access an account picker before going to a restricted Google Form.
Located at http://nonnymoose.gitlab.io/formaccount.
